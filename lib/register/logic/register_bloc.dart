import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:my_bloc_demo/Util/authUtil.dart';
import 'package:my_bloc_demo/Util/util.dart';
import 'package:my_bloc_demo/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

enum Event { register }

class RegisterBloc extends BlocBase {
  /// 输入信号
  PublishSubject<String> emailInputSubject = PublishSubject<String>();
  ReplaySubject<String> passwordInputSubject = ReplaySubject<String>();
  ReplaySubject<String> repeatInputSubject = ReplaySubject<String>();

  TextEditingController emailTextC = TextEditingController();
  TextEditingController passwordTextC = TextEditingController();


  // 事件输入，触发业务逻辑
  final eventController = StreamController<Event>();

  // 注册状态输出
  final _statusController = StreamController<RegisterStatusInfo>.broadcast();

  Stream<RegisterStatusInfo> get registerStatus => _statusController.stream;

  /// 验证结果
  late Stream<ValidType> emailValidResult;
  late Stream<ValidType> passwordValidResult;
  late Stream<ValidType> repeatValidResult;
  late Stream<bool> registerButtonEnable;

  RegisterBloc() {
    /// 邮箱验证
    inputValid();
    // 监听事件，触发具体业务逻辑
    eventController.stream.listen((event) {
      if (event == Event.register) {
        _registerWork();
      }
    });
  }

  _registerWork() async {
    _statusController
        .add(RegisterStatusInfo()
      ..status = RegisterStatus.loading);
    AuthResult authResult = await AuthUtil.register(
        email: emailTextC.text, password: passwordTextC.text);
    /// 注册loading  err  交由UI处理 界面交互和逻辑分离
    _statusController
        .add(RegisterStatusInfo()
      ..status = authResult.success ? RegisterStatus.success : RegisterStatus.error
      ..des = authResult.error);
  }

  registerClick() {
    eventController.add(Event.register);
  }

  @override
  void dispose() {
    eventController.close();
    _statusController.close();
  }

  inputValid() {
    emailValidResult = emailInputSubject.map((event) {
      // print("email map");
      if (event.isEmpty) return ValidType.none;
      // 填入内容 并且 email不合格
      return Util.checkEmail(event) ? ValidType.valid : ValidType.err;
    }).shareReplay(maxSize: 1); /// shareReplay 共享副作用 回放1个元素

    /// 密码验证
    passwordValidResult = passwordInputSubject.map((event) {
      if (event.isEmpty) return ValidType.none;

      /// 填入内容 并且 email不合格
      return event.length >= 6 ? ValidType.valid : ValidType.err;
    }).shareReplay(maxSize: 1);

    /// 确认密码验证  仅当两次密码有值时判断
    repeatValidResult = Rx.combineLatest2<String, String, ValidType>(
        passwordInputSubject, repeatInputSubject, (password, repeat) {
      // 两次密码任一为空时 不显示错误
      if (password.isEmpty || repeat.isEmpty) {
        return ValidType.none;
      }
      return password == repeat ? ValidType.valid : ValidType.err;
    }).shareReplay(maxSize: 1);

    /// 按钮禁用状态
    registerButtonEnable = Rx.combineLatest3(
        emailValidResult, passwordValidResult, repeatValidResult, (email,
        password,
        repeat,) {
      return email == ValidType.valid &&
          password == ValidType.valid &&
          repeat == ValidType.valid;
    });
  }
}

enum ValidType {
  none,
  err,
  valid,
}

class RegisterStatusInfo {
  RegisterStatus status = RegisterStatus.none;
  String? des;
}

enum RegisterStatus {
  none,
  loading,
  error,
  success,
}
