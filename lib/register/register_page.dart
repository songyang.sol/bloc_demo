import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_bloc_demo/bloc/bloc_provider.dart';
import 'package:my_bloc_demo/home/home_page.dart';
import 'package:my_bloc_demo/register/logic/register_bloc.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: const RegisterContent(),
        blocBuilder: () {
          return RegisterBloc();
        });
  }
}

class RegisterContent extends StatefulWidget {
  const RegisterContent({Key? key}) : super(key: key);

  @override
  State<RegisterContent> createState() => _RegisterContentState();
}

class _RegisterContentState extends State<RegisterContent> {
  late RegisterBloc bloc;
  StreamSubscription? sub;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      sub = bloc.registerStatus.listen((event) {
        /// 错误在UI中显示
        if (event.status == RegisterStatus.error) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(event.des ?? "")));
        } else if (event.status == RegisterStatus.success) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (c) {
            return const HomePage();
          }));
        }
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<RegisterBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: const Text('注册'),
        ),
        body: StreamBuilder<RegisterStatusInfo>(
            stream: bloc.registerStatus,
            builder: (context, snapshot) {
              return Stack(
                children: [
                  buildInput(context),
                  if (snapshot.data?.status == RegisterStatus.loading)
                    Positioned.fill(
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        child: Container(
                          alignment: Alignment.center,
                          child: const CupertinoActivityIndicator(
                            radius: 50,
                          ),
                        ),
                      ),
                    )
                ],
              );
            }));
  }

  buildInput(context) {
    RegisterBloc bloc = BlocProvider.of<RegisterBloc>(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 50),
          alignment: Alignment.center,
          child: Column(children: [
            StreamBuilder<ValidType>(
                stream: bloc.emailValidResult,
                initialData: ValidType.none,
                builder: (context, snapshot) {
                  return TextField(
                    controller: bloc.emailTextC,
                    decoration: InputDecoration(
                        hintText: "邮箱",
                        errorText: (snapshot.data == ValidType.err)
                            ? '请输入正确的邮箱'
                            : null),
                    onChanged: bloc.emailInputSubject.add,
                    // controller: nameController,
                  );
                }),
            StreamBuilder<ValidType>(
                stream: bloc.passwordValidResult,
                initialData: ValidType.none,
                builder: (context, snapshot) {
                  return TextField(
                    obscureText: true,
                    controller: bloc.passwordTextC,
                    decoration: InputDecoration(
                        hintText: "密码",
                        errorText:
                        (snapshot.data == ValidType.err) ? '密码最少6位' : null),
                    onChanged: bloc.passwordInputSubject.add,
                    // controller: pwController,
                  );
                }),
            StreamBuilder<ValidType>(
                stream: bloc.repeatValidResult,
                initialData: ValidType.none,
                builder: (context, snapshot) {
                  return TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: "确认密码",
                        errorText: (snapshot.data == ValidType.err)
                            ? '两次密码不一致'
                            : null),
                    onChanged: bloc.repeatInputSubject.add,
                    // controller: pwController,
                  );
                }),
            const SizedBox(
              height: 50,
            ),
            StreamBuilder<bool>(
                stream: bloc.registerButtonEnable,
                initialData: false,
                builder: (context, snapshot) {
                  return ElevatedButton(
                    onPressed: snapshot.data == true
                        ? () async {
                      bloc.registerClick();
                    }
                        : null,
                    child: const Text('注册'),
                  );
                }),
          ])),
    );
  }
}
