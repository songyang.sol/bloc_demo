class Util {

  static bool checkEmail(String? string) {
    if (string == null) return false;
    RegExp exp = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$",
        caseSensitive: false);
    return exp.hasMatch(string);
  }

}