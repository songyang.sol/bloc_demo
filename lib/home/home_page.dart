import 'package:flutter/material.dart';
import 'package:my_bloc_demo/register/register_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:const Text('home'),),
      body: Center(child:
        ElevatedButton(child: const Text('Logout'),onPressed: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (c) {
            return const RegisterPage();
          }));
        },),),
    );
  }
}
